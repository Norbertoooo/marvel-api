FROM openjdk:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/marvel-api-0.0.1-SNAPSHOT-standalone.jar /marvel-api/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/marvel-api/app.jar"]
